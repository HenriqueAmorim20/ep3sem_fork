class LineItem < ApplicationRecord
  belongs_to :produto
  belongs_to :cart

  def total_price
    produto.Preço.to_i * quantity.to_i
  end
end
