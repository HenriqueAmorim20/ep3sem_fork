Rails.application.routes.draw do
	
  resources :line_items
  resources :carts
  resources :produtos


  devise_for :users, controllers: {
    registrations: 'registrations'
  }

  
  root 'produtos#index'
  get 'line_items', to: 'line_items#create'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
