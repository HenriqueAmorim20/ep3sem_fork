class CreateProdutos < ActiveRecord::Migration[5.2]
  def change
    create_table :produtos do |t|
      t.string :Nome
      t.text :Descrição
      t.decimal :Preço, precision: 5, scale: 2, default: 0
      t.integer :Quantidade, scale: 2, default: 0
      t.decimal :Peso, precision: 5, scale: 2, default: 0

      t.timestamps
    end
  end
end
